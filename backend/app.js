// const express = require("express");
// const app = express();
// const mongoose = require("mongoose");
// const path = require("path");
// const cors = require("cors");
// require("dotenv").config();
// const authRoutes = require("./routes/authRoutes");
// const taskRoutes = require("./routes/taskRoutes");
// const profileRoutes = require("./routes/profileRoutes");

// app.use(express.json());
// app.use(cors());

// const mongoUrl = process.env.MONGODB_URL;
// mongoose.connect(mongoUrl, err => {(" mongodb+srv:parthsutarmongodb+srv://parthsutariya279:Parth@123@cluster0.gxef72y.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0iya279:Parth@123@cluster0.gxef72y.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0")
//   if (err) throw err;
//   console.log("Mongodb connected...");
// });

// app.use("/api/auth", authRoutes);
// app.use("/api/tasks", taskRoutes);
// app.use("/api/profile", profileRoutes);

// if (process.env.NODE_ENV === "production") {
//   app.use(express.static(path.resolve(__dirname, "../frontend/build")));
//   app.get("*", (req, res) => res.sendFile(path.resolve(__dirname, "../frontend/build/index.html")));
//   // console.log("helooooo1111");
// }

// const port = process.env.PORT || 5000;
// app.listen(port, () => {
//   console.log(`Backend is running on port ${port}`);
// });


require("dotenv").config();
console.log('MONGODB_URL:', process.env.MONGODB_URL);
console.log('PORT:', process.env.PORT);

// Rest of your code follows...



// require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const path = require("path");
const cors = require("cors");
const authRoutes = require("./routes/authRoutes");
const taskRoutes = require("./routes/taskRoutes");
const profileRoutes = require("./routes/profileRoutes");

// console.log('MONGODB_URL:', process.env.MONGODB_URL);
// console.log('PORT:', process.env.PORT);


app.use(express.json());
app.use(cors());


// const mongoUrl = process.env.MONGODB_URL;
// if (!mongoUrl) {
//   console.error('MONGODB_URL is not defined');
//   process.exit(1);
//   console.log("hiiii")
// }
const mongoUrl = process.env.MONGODB_URL;

if (!mongoUrl) {
  console.error('MONGODB_URL environment variable is not defined');
  process.exit(1); 
  // console.log("hiii")
}


mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Mongodb connected..."))
  .catch(err => console.error("Error connecting to MongoDB:", err.message));
  


app.use("/api/auth", authRoutes);
app.use("/api/tasks", taskRoutes);
app.use("/api/profile", profileRoutes);


if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.resolve(__dirname, "../frontend/build")));
  app.get("*", (req, res) => res.sendFile(path.resolve(__dirname, "../frontend/build/index.html")));
   console.log("hiiiiiiii")
}

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Backend is running on port ${port}`);
});
