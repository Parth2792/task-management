const express = require("express");
const router = express.Router();
const { getTasksList, getTaskDetails, createTask, updateTask, deleteTask } = require("../controllers/taskControllers");
const { verifyAccessToken } = require("../middlewares.js");

// Routes beginning with /api/tasks
router.get("/", verifyAccessToken, getTasksList);
router.get("/:taskId", verifyAccessToken, getTaskDetails);
router.post("/", verifyAccessToken, createTask);
router.put("/:taskId", verifyAccessToken, updateTask);
router.delete("/:taskId", verifyAccessToken, deleteTask);

module.exports = router;
