const Task = require("../models/Task");
const { validateObjectId } = require("../utils/validation");


exports.getTasksList = async (req, res) => {
  try {
    const tasks = await Task.find({ user: req.user.id });
    res.status(200).json({ tasks, status: true, msg: "Tasks found successfully.." });
  }
  catch (err) {
    console.error(err);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
}

exports.getTaskDetails = async (req, res) => {
  try {
    if (!validateObjectId(req.params.taskId)) {
      return res.status(400).json({ status: false, msg: "Task id not valid" });
    }

    const task = await Task.findOne({ user: req.user.id, _id: req.params.taskId });
    if (!task) {
      return res.status(400).json({ status: false, msg: "No task found.." });
    }
    res.status(200).json({ task, status: true, msg: "Task found successfully.." });
  }
  catch (err) {
    console.error(err);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
}

exports.createTask = async (req, res) => {
  try {
    let bodyData = req.body
    if (!bodyData.title || !bodyData.title.trim()) {
      return res.status(400).json({ status: false, msg: "Title of task not found" });
    }else if (!bodyData.description || !bodyData.description.trim()) {
      return res.status(400).json({ status: false, msg: "Description of task not found" });
    } else if (!bodyData.status || !bodyData.status.trim()) {
      return res.status(400).json({ status: false, msg: "Status of task not found" });
    } else{
      let userBody={
        user: req.user.id,
        description: bodyData.description,
        status:bodyData.status,
        title:bodyData.title
      }
      const task = await Task.create(userBody);
      res.status(200).json({ task, status: true, msg: "Task created successfully.." });
    }
    } catch (err) {
      console.error(err);
      return res.status(500).json({ status: false, msg: "Internal Server Error" });
    }
}

exports.updateTask = async (req, res) => {
  try {
    let bodyData = req.body
    if (!bodyData.title || !bodyData.title.trim()) {
      return res.status(400).json({ status: false, msg: "Title of task not found" });
    }else if (!bodyData.description || !bodyData.description.trim()) {
      return res.status(400).json({ status: false, msg: "Description of task not found" });
    } else if (!bodyData.status || !bodyData.status.trim()) {
      return res.status(400).json({ status: false, msg: "Status of task not found" });
    } else if (!validateObjectId(req.params.taskId)) {
      return res.status(400).json({ status: false, msg: "Task id not valid" });
    } else {
      let task = await Task.findById(req.params.taskId);
      if (!task) {
        return res.status(400).json({ status: false, msg: "Task with given id not found" });
      }
      if (task.user != req.user.id) {
        return res.status(403).json({ status: false, msg: "You can't update task of another user" });
      }
  
      let userBody = {
        user: req.user.id,
        description: bodyData.description,
        status: bodyData.status,
        title: bodyData.title,
      };
      task = await Task.findByIdAndUpdate(req.params.taskId,userBody, { new: true });
      res.status(200).json({ task, status: true, msg: "Task updated successfully.." });
    }
  }
  catch (err) {
    console.error(err);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
}


exports.deleteTask = async (req, res) => {
  try {
    if (!validateObjectId(req.params.taskId)) {
      return res.status(400).json({ status: false, msg: "Task id not valid" });
    }

    let task = await Task.findById(req.params.taskId);
    if (!task) {
      return res.status(400).json({ status: false, msg: "Task with given id not found" });
    }

    if (task.user != req.user.id) {
      return res.status(403).json({ status: false, msg: "You can't delete task of another user" });
    }

    await Task.findByIdAndDelete(req.params.taskId);
    res.status(200).json({ status: true, msg: "Task deleted successfully.." });
  }
  catch (err) {
    console.error(err);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
}